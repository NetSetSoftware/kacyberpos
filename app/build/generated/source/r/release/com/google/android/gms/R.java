/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.android.gms;

public final class R {
    public static final class attr {
        public static final int ambientEnabled = 0x7f04002f;
        public static final int buttonSize = 0x7f040054;
        public static final int cameraBearing = 0x7f040059;
        public static final int cameraMaxZoomPreference = 0x7f04005a;
        public static final int cameraMinZoomPreference = 0x7f04005b;
        public static final int cameraTargetLat = 0x7f04005c;
        public static final int cameraTargetLng = 0x7f04005d;
        public static final int cameraTilt = 0x7f04005e;
        public static final int cameraZoom = 0x7f04005f;
        public static final int circleCrop = 0x7f04007f;
        public static final int colorScheme = 0x7f040096;
        public static final int imageAspectRatio = 0x7f0400fb;
        public static final int imageAspectRatioAdjust = 0x7f0400fc;
        public static final int latLngBoundsNorthEastLatitude = 0x7f04010a;
        public static final int latLngBoundsNorthEastLongitude = 0x7f04010b;
        public static final int latLngBoundsSouthWestLatitude = 0x7f04010c;
        public static final int latLngBoundsSouthWestLongitude = 0x7f04010d;
        public static final int liteMode = 0x7f040159;
        public static final int mapType = 0x7f04015c;
        public static final int scopeUris = 0x7f040186;
        public static final int uiCompass = 0x7f0401f1;
        public static final int uiMapToolbar = 0x7f0401f2;
        public static final int uiRotateGestures = 0x7f0401f3;
        public static final int uiScrollGestures = 0x7f0401f4;
        public static final int uiTiltGestures = 0x7f0401f5;
        public static final int uiZoomControls = 0x7f0401f6;
        public static final int uiZoomGestures = 0x7f0401f7;
        public static final int useViewLifecycle = 0x7f0401fa;
        public static final int zOrderOnTop = 0x7f04020d;
    }
    public static final class color {
        public static final int common_google_signin_btn_text_dark = 0x7f060039;
        public static final int common_google_signin_btn_text_dark_default = 0x7f06003a;
        public static final int common_google_signin_btn_text_dark_disabled = 0x7f06003b;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f06003c;
        public static final int common_google_signin_btn_text_dark_pressed = 0x7f06003d;
        public static final int common_google_signin_btn_text_light = 0x7f06003e;
        public static final int common_google_signin_btn_text_light_default = 0x7f06003f;
        public static final int common_google_signin_btn_text_light_disabled = 0x7f060040;
        public static final int common_google_signin_btn_text_light_focused = 0x7f060041;
        public static final int common_google_signin_btn_text_light_pressed = 0x7f060042;
        public static final int common_google_signin_btn_tint = 0x7f060043;
        public static final int place_autocomplete_prediction_primary_text = 0x7f060081;
        public static final int place_autocomplete_prediction_primary_text_highlight = 0x7f060082;
        public static final int place_autocomplete_prediction_secondary_text = 0x7f060083;
        public static final int place_autocomplete_search_hint = 0x7f060084;
        public static final int place_autocomplete_search_text = 0x7f060085;
        public static final int place_autocomplete_separator = 0x7f060086;
    }
    public static final class dimen {
        public static final int place_autocomplete_button_padding = 0x7f0700e4;
        public static final int place_autocomplete_powered_by_google_height = 0x7f0700e5;
        public static final int place_autocomplete_powered_by_google_start = 0x7f0700e6;
        public static final int place_autocomplete_prediction_height = 0x7f0700e7;
        public static final int place_autocomplete_prediction_horizontal_margin = 0x7f0700e8;
        public static final int place_autocomplete_prediction_primary_text = 0x7f0700e9;
        public static final int place_autocomplete_prediction_secondary_text = 0x7f0700ea;
        public static final int place_autocomplete_progress_horizontal_margin = 0x7f0700eb;
        public static final int place_autocomplete_progress_size = 0x7f0700ec;
        public static final int place_autocomplete_separator_start = 0x7f0700ed;
    }
    public static final class drawable {
        public static final int common_full_open_on_phone = 0x7f080070;
        public static final int common_google_signin_btn_icon_dark = 0x7f080071;
        public static final int common_google_signin_btn_icon_dark_focused = 0x7f080072;
        public static final int common_google_signin_btn_icon_dark_normal = 0x7f080073;
        public static final int common_google_signin_btn_icon_dark_normal_background = 0x7f080074;
        public static final int common_google_signin_btn_icon_disabled = 0x7f080075;
        public static final int common_google_signin_btn_icon_light = 0x7f080076;
        public static final int common_google_signin_btn_icon_light_focused = 0x7f080077;
        public static final int common_google_signin_btn_icon_light_normal = 0x7f080078;
        public static final int common_google_signin_btn_icon_light_normal_background = 0x7f080079;
        public static final int common_google_signin_btn_text_dark = 0x7f08007a;
        public static final int common_google_signin_btn_text_dark_focused = 0x7f08007b;
        public static final int common_google_signin_btn_text_dark_normal = 0x7f08007c;
        public static final int common_google_signin_btn_text_dark_normal_background = 0x7f08007d;
        public static final int common_google_signin_btn_text_disabled = 0x7f08007e;
        public static final int common_google_signin_btn_text_light = 0x7f08007f;
        public static final int common_google_signin_btn_text_light_focused = 0x7f080080;
        public static final int common_google_signin_btn_text_light_normal = 0x7f080081;
        public static final int common_google_signin_btn_text_light_normal_background = 0x7f080082;
        public static final int googleg_disabled_color_18 = 0x7f080278;
        public static final int googleg_standard_color_18 = 0x7f080279;
        public static final int places_ic_clear = 0x7f0802ad;
        public static final int places_ic_search = 0x7f0802ae;
        public static final int powered_by_google_dark = 0x7f0802af;
        public static final int powered_by_google_light = 0x7f0802b0;
    }
    public static final class id {
        public static final int adjust_height = 0x7f090029;
        public static final int adjust_width = 0x7f09002a;
        public static final int auto = 0x7f090039;
        public static final int center = 0x7f090067;
        public static final int dark = 0x7f090092;
        public static final int date = 0x7f090093;
        public static final int hybrid = 0x7f0900e9;
        public static final int icon_only = 0x7f0900ee;
        public static final int light = 0x7f09010a;
        public static final int none = 0x7f09014d;
        public static final int normal = 0x7f09014e;
        public static final int place_autocomplete_clear_button = 0x7f090166;
        public static final int place_autocomplete_powered_by_google = 0x7f090167;
        public static final int place_autocomplete_prediction_primary_text = 0x7f090168;
        public static final int place_autocomplete_prediction_secondary_text = 0x7f090169;
        public static final int place_autocomplete_progress = 0x7f09016a;
        public static final int place_autocomplete_search_button = 0x7f09016b;
        public static final int place_autocomplete_search_input = 0x7f09016c;
        public static final int place_autocomplete_separator = 0x7f09016d;
        public static final int radio = 0x7f090185;
        public static final int satellite = 0x7f09019a;
        public static final int standard = 0x7f0901d8;
        public static final int terrain = 0x7f0901ea;
        public static final int text = 0x7f0901eb;
        public static final int text2 = 0x7f0901ec;
        public static final int toolbar = 0x7f0901ff;
        public static final int wide = 0x7f090237;
        public static final int wrap_content = 0x7f09023a;
    }
    public static final class integer {
        public static final int google_play_services_version = 0x7f0a000c;
    }
    public static final class layout {
        public static final int place_autocomplete_fragment = 0x7f0b0077;
        public static final int place_autocomplete_item_powered_by_google = 0x7f0b0078;
        public static final int place_autocomplete_item_prediction = 0x7f0b0079;
        public static final int place_autocomplete_progress = 0x7f0b007a;
    }
    public static final class string {
        public static final int common_google_play_services_enable_button = 0x7f100050;
        public static final int common_google_play_services_enable_text = 0x7f100051;
        public static final int common_google_play_services_enable_title = 0x7f100052;
        public static final int common_google_play_services_install_button = 0x7f100053;
        public static final int common_google_play_services_install_text = 0x7f100054;
        public static final int common_google_play_services_install_title = 0x7f100055;
        public static final int common_google_play_services_notification_channel_name = 0x7f100056;
        public static final int common_google_play_services_notification_ticker = 0x7f100057;
        public static final int common_google_play_services_unknown_issue = 0x7f100058;
        public static final int common_google_play_services_unsupported_text = 0x7f100059;
        public static final int common_google_play_services_update_button = 0x7f10005a;
        public static final int common_google_play_services_update_text = 0x7f10005b;
        public static final int common_google_play_services_update_title = 0x7f10005c;
        public static final int common_google_play_services_updating_text = 0x7f10005d;
        public static final int common_google_play_services_wear_update_text = 0x7f10005e;
        public static final int common_open_on_phone = 0x7f10005f;
        public static final int common_signin_button_text = 0x7f100060;
        public static final int common_signin_button_text_long = 0x7f100061;
        public static final int fcm_fallback_notification_channel_label = 0x7f100329;
        public static final int place_autocomplete_clear_button = 0x7f10037f;
        public static final int place_autocomplete_search_hint = 0x7f100380;
    }
    public static final class styleable {
        public static final int[] LoadingImageView = { 0x7f04007f, 0x7f0400fb, 0x7f0400fc };
        public static final int LoadingImageView_circleCrop = 0;
        public static final int LoadingImageView_imageAspectRatio = 1;
        public static final int LoadingImageView_imageAspectRatioAdjust = 2;
        public static final int[] MapAttrs = { 0x7f04002f, 0x7f040059, 0x7f04005a, 0x7f04005b, 0x7f04005c, 0x7f04005d, 0x7f04005e, 0x7f04005f, 0x7f04010a, 0x7f04010b, 0x7f04010c, 0x7f04010d, 0x7f040159, 0x7f04015c, 0x7f0401f1, 0x7f0401f2, 0x7f0401f3, 0x7f0401f4, 0x7f0401f5, 0x7f0401f6, 0x7f0401f7, 0x7f0401fa, 0x7f04020d };
        public static final int MapAttrs_ambientEnabled = 0;
        public static final int MapAttrs_cameraBearing = 1;
        public static final int MapAttrs_cameraMaxZoomPreference = 2;
        public static final int MapAttrs_cameraMinZoomPreference = 3;
        public static final int MapAttrs_cameraTargetLat = 4;
        public static final int MapAttrs_cameraTargetLng = 5;
        public static final int MapAttrs_cameraTilt = 6;
        public static final int MapAttrs_cameraZoom = 7;
        public static final int MapAttrs_latLngBoundsNorthEastLatitude = 8;
        public static final int MapAttrs_latLngBoundsNorthEastLongitude = 9;
        public static final int MapAttrs_latLngBoundsSouthWestLatitude = 10;
        public static final int MapAttrs_latLngBoundsSouthWestLongitude = 11;
        public static final int MapAttrs_liteMode = 12;
        public static final int MapAttrs_mapType = 13;
        public static final int MapAttrs_uiCompass = 14;
        public static final int MapAttrs_uiMapToolbar = 15;
        public static final int MapAttrs_uiRotateGestures = 16;
        public static final int MapAttrs_uiScrollGestures = 17;
        public static final int MapAttrs_uiTiltGestures = 18;
        public static final int MapAttrs_uiZoomControls = 19;
        public static final int MapAttrs_uiZoomGestures = 20;
        public static final int MapAttrs_useViewLifecycle = 21;
        public static final int MapAttrs_zOrderOnTop = 22;
        public static final int[] SignInButton = { 0x7f040054, 0x7f040096, 0x7f040186 };
        public static final int SignInButton_buttonSize = 0;
        public static final int SignInButton_colorScheme = 1;
        public static final int SignInButton_scopeUris = 2;
    }
}
