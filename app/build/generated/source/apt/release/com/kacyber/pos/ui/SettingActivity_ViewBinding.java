// Generated code from Butter Knife. Do not modify!
package com.kacyber.pos.ui;

import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.kacyber.pos.R;
import com.kacyber.pos.ui.base.BaseActivity_ViewBinding;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SettingActivity_ViewBinding extends BaseActivity_ViewBinding {
  private SettingActivity target;

  private View view2131296636;

  private View view2131296264;

  private View view2131296371;

  private View view2131296638;

  private View view2131296637;

  private View view2131296742;

  private View view2131296485;

  private View view2131296366;

  private View view2131296706;

  @UiThread
  public SettingActivity_ViewBinding(SettingActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SettingActivity_ViewBinding(final SettingActivity target, View source) {
    super(target, source);

    this.target = target;

    View view;
    target.mVersionCode = Utils.findRequiredViewAsType(source, R.id.versionCode, "field 'mVersionCode'", TextView.class);
    view = Utils.findRequiredView(source, R.id.printersRL, "field 'mPrintersView' and method 'onClick'");
    target.mPrintersView = view;
    view2131296636 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    target.cacheTV = Utils.findRequiredViewAsType(source, R.id.cacheTV, "field 'cacheTV'", TextView.class);
    view = Utils.findRequiredView(source, R.id.NotificationRL, "method 'onClick'");
    view2131296264 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.cleaCacheRL, "method 'onClick'");
    view2131296371 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.privacyRL, "method 'onClick'");
    view2131296638 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.privacyPolicyRL, "method 'onClick'");
    view2131296637 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.termsOfsericeRL, "method 'onClick'");
    view2131296742 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.helpRL, "method 'onClick'");
    view2131296485 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.checkUpdateRL, "method 'onClick'");
    view2131296366 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.signOutBT, "method 'onClick'");
    view2131296706 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  public void unbind() {
    SettingActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mVersionCode = null;
    target.mPrintersView = null;
    target.cacheTV = null;

    view2131296636.setOnClickListener(null);
    view2131296636 = null;
    view2131296264.setOnClickListener(null);
    view2131296264 = null;
    view2131296371.setOnClickListener(null);
    view2131296371 = null;
    view2131296638.setOnClickListener(null);
    view2131296638 = null;
    view2131296637.setOnClickListener(null);
    view2131296637 = null;
    view2131296742.setOnClickListener(null);
    view2131296742 = null;
    view2131296485.setOnClickListener(null);
    view2131296485 = null;
    view2131296366.setOnClickListener(null);
    view2131296366 = null;
    view2131296706.setOnClickListener(null);
    view2131296706 = null;

    super.unbind();
  }
}
