/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.kacyber.pos;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.kacyber.pos";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 29;
  public static final String VERSION_NAME = "1.29.0";
  // Fields from build type: debug
  public static final String SERVER_URL = "https://www.kacyber.com/";
}
