package com.kacyber.pos.ui;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.kacyber.pos.R;
import com.kacyber.pos.ui.base.BaseActivity;


public class WebViewActivity extends BaseActivity {
    private String url;
    private String urlType;
    private String title;

    private final String googleDocs = "https://docs.google.com/viewer?url=";
    private WebView webview;

    @SuppressLint({"NewApi", "SetJavaScriptEnabled"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        webview = findViewById(R.id.webview);
        url = getIntent().getStringExtra("url");
        urlType = getIntent().getStringExtra("urltype");
        title = getIntent().getStringExtra("title");
        initiWebView();
        setTitle(title);
    }

    private void initiWebView() {
        webview.getSettings().setDomStorageEnabled(true);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                // startProgressDialog();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                // stopProgressDialog();
            }
        });

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            webview.getSettings().setDatabasePath("/data/data/" + webview.getContext().getPackageName() + "/databases/");
        }
        if (urlType.equals("pdf")) {
            webview.getSettings().setAllowFileAccess(true);
            webview.getSettings().setLoadsImagesAutomatically(true);
            webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
            webview.getSettings().setLoadWithOverviewMode(true);
            webview.getSettings().setUseWideViewPort(true);
            webview.getSettings().setTextZoom(10);
            webview.loadUrl(googleDocs + url);
        } else {
            webview.loadUrl(url);
        }
    }

    @Override
    protected int getLayoutResID() {
        return R.layout.activity_web_view;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        mTitleView.setText(title);
        //   setTitle(title);
    }

    @Override
    public void onBackPressed() {
        if (webview.canGoBack()) {
            webview.goBack();
        } else {
            super.onBackPressed();
        }
    }
}
